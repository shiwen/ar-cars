﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ViewHandler : MonoBehaviour
{
	[Header ("UI Views")]
	public GameObject parentUI;
	public Toggle toggleInfo;
	public Button nextBtn;
	public Button prevBtn;


	[System.Serializable]
	public class Cars
	{
		public GameObject car;
		public Canvas canvas3d;
	}

	public List<Cars> carList;

	[Header ("Particles")]
	public GameObject particleEffect;
	public GameObject particleSpawnPoint;



}
