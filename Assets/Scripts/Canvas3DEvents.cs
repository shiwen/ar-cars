﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Canvas3DEvents : MonoBehaviour
{
	[System.Serializable]
	public class HotSpot
	{
		public GameObject parentHotspot;
		public Toggle infoToggle;
		public GameObject textPanel;
	}


	public List<HotSpot> hotspotList;

	void Start ()
	{
		//Loop through and assign button events to all hotspots toggles

		for (int i = 0; i < hotspotList.Count; i++) {
			int index = i;
			hotspotList [index].infoToggle.onValueChanged.AddListener ((bool isOn) => {
				if (isOn) {
					LeanTween.scaleX (hotspotList [index].textPanel, 1f, 0.5f);
				} else {
					LeanTween.scaleX (hotspotList [index].textPanel, 0f, 0.5f).setOnComplete (() => {
						hotspotList [index].infoToggle.isOn = false;
					});

				}
			});
		}


	}
}
