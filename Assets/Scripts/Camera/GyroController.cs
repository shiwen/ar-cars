// ***********************************************************
// Written by Heyworks Unity Studio http://unity.heyworks.com/
// ***********************************************************
using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// Gyroscope controller that works with any device orientation.
/// </summary>
public class GyroController : MonoBehaviour
{
    #region [Private fields]
    private bool gyroEnabled = true;
    private const float lowPassFilterFactor = 0.2f;

    private readonly Quaternion baseIdentity = Quaternion.Euler(90, 0, 0);
    private readonly Quaternion landscapeRight = Quaternion.Euler(0, 0, 90);
    private readonly Quaternion landscapeLeft = Quaternion.Euler(0, 0, -90);
    private readonly Quaternion upsideDown = Quaternion.Euler(0, 0, 180);

    private Quaternion cameraBase = Quaternion.identity;
    private Quaternion calibration = Quaternion.identity;
    private Quaternion baseOrientation = Quaternion.Euler(90, 0, 0);
    private Quaternion baseOrientationRotationFix = Quaternion.identity;

    private Quaternion referanceRotation = Quaternion.identity;

    private Quaternion setHorizon = Quaternion.identity;
    private float setValue;

    private float delta;

    public bool isPanoramic;
    public float minHori;
    public float maxHori;
    public float minTlit;
    public float maxTlit;
    private float convertToNegativeValueY;

    //  private bool debug = true;

    #endregion

    #region [Unity events]

	private static GyroController s_Instance = null;

	public static GyroController instance {
		get {
			if (s_Instance == null) {
				s_Instance = FindObjectOfType (typeof(GyroController)) as GyroController;
			}
			return s_Instance;
		}
	}

    protected void Start()
    {
        Input.gyro.enabled = true;
        AttachGyro();

        if (minHori < 0)
        {
            minHori = minHori + 360;
            convertToNegativeValueY = minHori - 360;
        }
    }

    protected void Update()
    {
        if (!gyroEnabled)
            return;

        transform.rotation = Quaternion.Slerp (transform.rotation, setHorizon * 
            cameraBase * (ConvertRotation (referanceRotation * Input.gyro.attitude) * GetRotFix ()), lowPassFilterFactor);

        //**For Panoramic clamping angles**//

        float angleY = transform.localEulerAngles.y;
        angleY = (angleY > 180) ? angleY - 360 : angleY;
        angleY = Mathf.Clamp(angleY, convertToNegativeValueY, maxHori);


        float angleX = transform.localEulerAngles.x;
        angleX = (angleX > 180) ? angleX - 360 : angleX;
        angleX = Mathf.Clamp(angleX, minTlit, maxTlit);

        if (isPanoramic)
        {
            transform.localEulerAngles = new Vector3(angleX, angleY, 0f);
        }    
    }

    /*protected void OnGUI()
	{
		if (!debug)
			return;

		GUILayout.Label("Orientation: " + Screen.orientation);
		GUILayout.Label("Calibration: " + calibration);
		GUILayout.Label("Camera base: " + cameraBase);
		GUILayout.Label("input.gyro.attitude: " + Input.gyro.attitude);
		GUILayout.Label("transform.rotation: " + transform.rotation);

		if (GUILayout.Button("On/off gyro: " + Input.gyro.enabled, GUILayout.Height(100)))
		{
			Input.gyro.enabled = !Input.gyro.enabled;
		}

		if (GUILayout.Button("On/off gyro controller: " + gyroEnabled, GUILayout.Height(100)))
		{
			if (gyroEnabled)
			{
				DetachGyro();
			}
			else
			{
				AttachGyro();
			}
		}

		if (GUILayout.Button("Update gyro calibration (Horizontal only)", GUILayout.Height(80)))
		{
			UpdateCalibration(true);
		}

		if (GUILayout.Button("Update camera base rotation (Horizontal only)", GUILayout.Height(80)))
		{
			UpdateCameraBaseRotation(true);
		}

		if (GUILayout.Button("Reset base orientation", GUILayout.Height(80)))
		{
			ResetBaseOrientation();
		}

		if (GUILayout.Button("Reset camera rotation", GUILayout.Height(80)))
		{
			transform.rotation = Quaternion.identity;
		}
	}
	*/
    #endregion

    #region [Public methods]

    /// <summary>
    /// Attaches gyro controller to the transform.
    /// </summary>
    private void AttachGyro()
    {
        gyroEnabled = true;
        ResetBaseOrientation();
        UpdateCalibration(true);
        UpdateCameraBaseRotation(true);
        RecalculateReferenceRotation();
    }

    /// <summary>
    /// Detaches gyro controller from the transform
    /// </summary>
    private void DetachGyro()
    {
        gyroEnabled = false;
    }

    #endregion

    #region [Private methods]

    /// <summary>
    /// Update the gyro calibration.
    /// </summary>
    private void UpdateCalibration(bool onlyHorizontal)
    {
        if (onlyHorizontal)
        {
            var fw = (Input.gyro.attitude) * (-Vector3.forward);
            fw.z = 0;
            if (fw == Vector3.zero)
            {
                calibration = Quaternion.identity;
            }
            else
            {
                calibration = (Quaternion.FromToRotation(baseOrientationRotationFix * Vector3.up, fw));
            }
        }
        else
        {
            calibration = Input.gyro.attitude;
        }
    }

    /// <summary>
    /// Update the camera base rotation.
    /// </summary>
    /// <param name='onlyHorizontal'>
    /// Only y rotation.
    /// </param>
    private void UpdateCameraBaseRotation(bool onlyHorizontal)
    {
        if (onlyHorizontal)
        {
            var fw = transform.forward;
            fw.y = 0;
            if (fw == Vector3.zero)
            {
                cameraBase = Quaternion.identity;
            }
            else
            {
                cameraBase = Quaternion.FromToRotation(Vector3.forward, fw);
            }
        }
        else
        {
            cameraBase = transform.rotation;
        }

        Debug.Log("cameraBase: " + cameraBase);
    }

    /// <summary>
    /// Converts the rotation from right handed to left handed.
    /// </summary>
    /// <returns>
    /// The result rotation.
    /// </returns>
    /// <param name='q'>
    /// The rotation to convert.
    /// </param>
    private static Quaternion ConvertRotation(Quaternion q)
    {
        return new Quaternion(q.x, q.y, -q.z, -q.w);
    }

    /// <summary>
    /// Gets the rot fix for different orientations.
    /// </summary>
    /// <returns>
    /// The rot fix.
    /// </returns>
    private Quaternion GetRotFix()
    {
#if UNITY_3_5
		if (Screen.orientation == ScreenOrientation.Portrait)
			return Quaternion.identity;
		
		if (Screen.orientation == ScreenOrientation.LandscapeLeft || Screen.orientation == ScreenOrientation.Landscape)
			return landscapeLeft;
				
		if (Screen.orientation == ScreenOrientation.LandscapeRight)
			return landscapeRight;
				
		if (Screen.orientation == ScreenOrientation.PortraitUpsideDown)
			return upsideDown;
		return Quaternion.identity;
#else
        return Quaternion.identity;
#endif
    }

    /// <summary>
    /// Recalculates reference system.
    /// </summary>
    private void ResetBaseOrientation()
    {
        baseOrientationRotationFix = GetRotFix();
        baseOrientation = baseOrientationRotationFix * baseIdentity;
    }

    /// <summary>
    /// Recalculates reference rotation.
    /// </summary>
    private void RecalculateReferenceRotation()
    {
        referanceRotation = Quaternion.Inverse(baseOrientation) * Quaternion.Inverse(calibration);
        Debug.Log(referanceRotation);
    }

    public void DragView (float delta)
    {
        if(isPanoramic)
        {
            if (transform.rotation.eulerAngles.y == minHori && delta < 0)
            {
                delta = 0;
            }

            if (transform.rotation.eulerAngles.y == maxHori && delta > 0)
            {
                delta = 0;
            }          
        }

        setValue += delta;

        if (setValue > 360)
        {
            setValue = 0;
        }
        else if (setValue < 0)
        {
            setValue = 360;
        }

        setHorizon = Quaternion.Euler (0,setValue, 0);
        return;
    }

    public void BeginDrag()
    {
        return;
    }

    public void EndDrag()
    {
        return;
    }

    public void Drag()
    {

#if UNITY_EDITOR
        float x = Input.GetAxis("Mouse X");
        Quaternion rotation = Quaternion.identity;
        if (x > 0.1f)
        {
            transform.eulerAngles += new Vector3(0, -50, 0) * Time.deltaTime;
        }

        else if (x < -0.1f)
        {
            transform.eulerAngles += new Vector3(0, 50, 0) * Time.deltaTime;
        }

#elif UNITY_ANDROID || UNITY_IOS
        Touch touch = Input.GetTouch (0);
        DragView(-touch.deltaPosition.x / 10f);
        
#endif
    }
    #endregion
}
