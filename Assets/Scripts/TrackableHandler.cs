﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class TrackableHandler : MonoBehaviour,ITrackableEventHandler
{

	#region Declarations

	private TrackableBehaviour mTrackableBehaviour;
	private ViewHandler viewHandler;
	private bool objTrackingFound = false;


	#endregion

	void Awake ()
	{
		if (GetComponent<ViewHandler> () != null) {
			viewHandler = GetComponent<ViewHandler> ();
		} else {
			Debug.Log ("ViewHandler is missing! Attach script to this object");
		}

		mTrackableBehaviour = GetComponent<TrackableBehaviour> ();
		if (mTrackableBehaviour) {
			mTrackableBehaviour.RegisterTrackableEventHandler (this);
		}
	}

	void Start ()
	{
		InitGeneralButtonEvents ();
	}

	void InitGeneralButtonEvents ()
	{
		viewHandler.toggleInfo.onValueChanged.AddListener ((bool isOn) => {


			//Grab the hotspot lists depending on which car is currently selected
			List<Canvas3DEvents.HotSpot> hotspotList;

			hotspotList = viewHandler.carList [(int)GameManager.Instance.carSelectionState].canvas3d.GetComponent<Canvas3DEvents> ().hotspotList;


			//Toggle on/off the hotspots
			foreach (Canvas3DEvents.HotSpot hotspot in hotspotList) {
				if (isOn) {
					hotspot.parentHotspot.SetActive (false);
				} else {
					hotspot.parentHotspot.SetActive (true);
				}
			}

		});

		viewHandler.prevBtn.onClick.AddListener (() => {
			if (objTrackingFound) {
				GameManager.Instance.carSelectionState -= 1;
				if ((int)GameManager.Instance.carSelectionState == -1) {
					int finalInt = GameManager.Instance.GetNumberOfCarSelections () - 1;
					GameManager.Instance.carSelectionState = (GameManager.CarSelection)finalInt;


				}
				ResetCars ();
				PlayCarEvents ((int)GameManager.Instance.carSelectionState);
			}
		});

		viewHandler.nextBtn.onClick.AddListener (() => {
			if (objTrackingFound) {
				GameManager.Instance.carSelectionState += 1;
				if ((int)GameManager.Instance.carSelectionState == GameManager.Instance.GetNumberOfCarSelections ()) {
					GameManager.Instance.carSelectionState = 0;
				}
				ResetCars ();
				PlayCarEvents ((int)GameManager.Instance.carSelectionState);
			}
		});
	}

	#region ITrackableEventHandler implementation

	void ITrackableEventHandler.OnTrackableStateChanged (TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
	{
		if (newStatus == TrackableBehaviour.Status.DETECTED ||
		    newStatus == TrackableBehaviour.Status.TRACKED ||
		    newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED) {
			OnTrackingFound ();
		} else {
			OnTrackingLost ();
		}
	}

	#endregion

	void OnTrackingFound ()
	{
		//Close UI if tracking found
		if (viewHandler.parentUI != null) {
			if (viewHandler.parentUI.activeInHierarchy) {
				viewHandler.parentUI.SetActive (false);

			}
		}
		objTrackingFound = true;

		Debug.Log ("Trackable [" + mTrackableBehaviour.TrackableName + "] found");

		PlayCarEvents ((int)GameManager.CarSelection.Car01);
	}

	void PlayCarEvents (int carNumber)
	{
		print (carNumber);
		GameObject go = Instantiate (viewHandler.particleEffect) as GameObject;

		go.transform.parent = viewHandler.particleSpawnPoint.transform;
		go.transform.position = viewHandler.particleSpawnPoint.transform.position;
		go.transform.localScale = viewHandler.particleEffect.transform.localScale;

		StartCoroutine (GameManager.Instance.PlaySFXCallback (GameManager.Instance.sfxList [0], () => {

			viewHandler.carList [carNumber].car.SetActive (true);
			SoundManager.PlaySFX (GameManager.Instance.sfxList [1]);
			//Tween car model after half the sound is played.
			LeanTween.scale (viewHandler.carList [carNumber].car, Vector3.one, 1.3f).setOnComplete (() => {
				Destroy (go, 0.3f);

				//grab the hotspot list, from the respective car
				List<Canvas3DEvents.HotSpot> hotspotList = viewHandler.carList [carNumber].canvas3d.GetComponent<Canvas3DEvents> ().hotspotList;

				for (int i = 0; i < hotspotList.Count; i++) {
					LeanTween.scale (hotspotList [i].parentHotspot, new Vector3 (-1f, 1f, 1f), 0.5f).setDelay (0.5f);
				}
			});
			

		}));
			
	}

	void ResetCars ()
	{
		//RESET EVERYTHING
		foreach (ViewHandler.Cars car in viewHandler.carList) {
			car.car.transform.localScale = Vector3.zero; //reset scale back to zero, for tween purposes
			car.car.SetActive (false); //Off objects

			foreach (Canvas3DEvents.HotSpot hotspot in car.canvas3d.GetComponent<Canvas3DEvents>().hotspotList) {
				hotspot.parentHotspot.transform.localScale = new Vector3 (0, 1f, 1f);
				hotspot.textPanel.transform.localScale = new Vector3 (0, 1f, 1f);
				hotspot.infoToggle.isOn = false;
			}

		}
	}

	void OnTrackingLost ()
	{
		ResetCars ();
		objTrackingFound = false;

		Debug.Log ("Trackable [" + mTrackableBehaviour.TrackableName + "] lost");


		//Enable UI when tracking is lost
		if (viewHandler.parentUI != null) {
			if (!viewHandler.parentUI.activeInHierarchy) {
				viewHandler.parentUI.SetActive (true);

			}
		}
	}
}
