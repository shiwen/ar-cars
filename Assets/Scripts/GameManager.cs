﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GameManager : Singleton<GameManager>
{
	
	public List<AudioClip> sfxList;
	public AudioClip bgm;


	public enum CarSelection
	{
		Car01 = 0,
		Car02 = 1,
		Car03 = 2
	}

	public CarSelection carSelectionState;

	void Start ()
	{
		SoundManager.PlayBGM (bgm, true, 1f);
	}

	public int GetNumberOfCarSelections ()
	{
		int length = Enum.GetValues (typeof(CarSelection)).Length;
		return length;
	}

	#region Audio Events

	public IEnumerator PlaySFXCallback (AudioClip audio, Action oncomplete)
	{
		SoundManager.PlaySFX (audio);
		yield return new WaitForSeconds (audio.length);
		if (oncomplete != null) {
			oncomplete ();
		}
	}


	#endregion

}
