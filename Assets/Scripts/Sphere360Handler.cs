﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sphere360Handler : MonoBehaviour
{
	public GameObject sphere360;
	public List<TextAsset> textAssetList;


	void Start ()
	{
		sphere360.GetComponent<MeshRenderer> ().material.mainTexture = LoadBytes ();
	}

	Texture2D LoadBytes ()
	{
		TextAsset tmp = textAssetList [0];

		Texture2D imgTexture = new Texture2D (0, 0, TextureFormat.RGB24, false);
		imgTexture.LoadImage (tmp.bytes);
		imgTexture.wrapMode = TextureWrapMode.Clamp;

		Resources.UnloadAsset (tmp);
		return imgTexture;
	}

}
