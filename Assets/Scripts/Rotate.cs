﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour
{

	public float speed;
	public Vector3 rotationAxis;
	
	// Update is called once per frame
	void Update ()
	{
		transform.Rotate (rotationAxis * speed, Space.Self);
	}
}
